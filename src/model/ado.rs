use serde::{Serialize, Deserialize};

#[derive(Serialize, Deserialize)]
pub struct Query {
    query: String
}

impl Query {
    pub fn new(q: String) -> Query {
        Query {
            query : q
        }
    }
}