use iced::{Application, Command, Clipboard, Element,
Text, Column, Settings, Scrollable, scrollable, text_input, Button,
button};
use reqwest::Client;
use crate::model::ado;

mod model;

fn main() -> iced::Result {
    ToAdo::run(Settings::default())
}

enum ToAdo {
    Syncing,
    Synced(State)
}

#[derive(Debug, Default)]
struct State {
    scroll: scrollable::State,
    input: text_input::State,
    input_value: String,
    counter: Counter
}

#[derive(Debug, Clone, Copy)]
enum Message {
    IncrementPressed,
    DecrementPressed,
    Synced(Result<AdoSync, AdoError>)
}

impl Application for ToAdo {
    type Executor = iced::executor::Default;
    type Message = Message;
    type Flags = ();

    fn new(_flags: ()) -> (ToAdo, Command<Message>) {
        (ToAdo::Synced(State::default()), Command::none())
    }

    fn title(&self) -> String {
        "TOADO".to_string()
    }

    fn update(&mut self, message: Message, _clipboard: &mut Clipboard) -> Command<Message> {
        match self {
            ToAdo::Syncing => {
                *self = ToAdo::Synced(State::default());
            }
            ToAdo::Synced(state) => {
                state.counter.update(message);
            }
            _ => ()
        };
        Command::none()
    }

    fn view(&mut self) -> Element<Message> {
        match self {
            ToAdo::Synced(state) => {
                let title = Text::new("Hello there");
            
        
                let column = Column::new()
                    .push(title);

                Scrollable::new(&mut state.scroll)
                    .push(column)
                    .push(state.counter.view())
                    .into()
            }
            _ => {
                let title = Text::new("Hello there");
            
                Column::new()
                    .push(title)
                    .into()
            }
        }   
    }
}

#[derive(Debug, Default)]
struct Counter {
    value: i32,
    increment_button: button::State,
    decrement_button: button::State
}

impl Counter {
    pub fn view(&mut self) -> Column<Message> {
        Column::new()
            .push(Button::new(&mut self.increment_button, Text::new("+"))
            .on_press(Message::IncrementPressed))
            .push(Text::new(&self.value.to_string()).size(50))
            .push(Button::new(&mut self.decrement_button, Text::new("-"))
            .on_press(Message::DecrementPressed))

    }

    pub fn update(&mut self, message: Message) {
        match message {
            Message::IncrementPressed => {
                self.value += 1;
            }
            Message::DecrementPressed => {
                self.value -= 1;
            }
            _ => ()
        }
    }
}

#[derive(Copy, Clone, Debug)]
struct AdoSync {

}

#[derive(Copy, Clone, Debug)]
enum AdoError {

}

impl AdoSync {
    // async fn initalSync() -> Result<AdoSync, AdoError> {

    // }
}

struct AdoClient {

}

impl AdoClient {
    async fn getEpics() {

    }

    async fn getTicket() {
        let q = "Select [System.Id] From WorkItems Where [System.WorkItemType] = Epic";
        let client = Client::new();

        client.post("https://mfaunmolvjri7ropl3ip5t3bxiif4jnol7atxsvb25xt5h74l5la@dev.azure.com/wexinc-gf-mwd/WMD/_apis/wit/wiql?api-version=6.0");
    }
}
